﻿

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MVTest.Controllers;
using MVTest.Helpers;
using MVTest.Models.ViewModel;
using MVTest.Services.Interfaces;
using System.Web.Mvc;

namespace MVTestTests.Controllers
{
    [TestClass]
    public class MatrixControllerTest
    {
        private MatrixController controller;
        private Mock<IMatrixService> mock;
        
        public MatrixControllerTest()
        {
            mock = new Mock<IMatrixService>();                   
            mock.Setup(a => a.ExportMatrixCvs(It.IsAny<string>(), It.IsAny<MatrixViewModel>())).Returns(
                new OperationDetail(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>()));

            mock.Setup(a => a.LoadMatrixCvs(It.IsAny<MatrixViewModel>())).Returns(
                new OperationDetail(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<MatrixViewModel>()));

            mock.Setup(a => a.TransformMatrix(It.IsAny<MatrixViewModel>())).Returns(
                new OperationDetail(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<MatrixViewModel>()));

            mock.Setup(a => a.RandomMatrix()).Returns(
               new OperationDetail(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<MatrixViewModel>()));

            controller = new MatrixController(mock.Object);
        }

        [TestMethod]
        public void IndexViewResultNotNull()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }        

        [TestMethod]
        public void LoadMatrixReturnViewResult()
        {
            ViewResult result = controller.LoadMatrix(new MatrixViewModel()) as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(MatrixViewModel));
        }

        [TestMethod]
        public void TransformMatrixReturnViewResult()
        {
            var result = controller.TransformMatrix(It.IsAny<MatrixViewModel>()) as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(MatrixViewModel));
        }

        [TestMethod]
        public void RandomMatrixReturnViewResult()
        {
            ViewResult result = controller.RandomMatrix() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(MatrixViewModel));
        }        
    }
}
