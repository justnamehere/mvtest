﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MVTest.Helpers;
using MVTest.Models;
using MVTest.Models.ViewModel;
using MVTest.Services;
using MVTest.Services.Interfaces;

namespace MVTestTests.Services
{
    [TestClass]
    public class MatrixServiceTest
    {
        private IMatrixService serv;

        public MatrixServiceTest()
        {
            serv = new MatrixService();
        }

        [TestMethod]
        public void RandomMatrixReturnOperationDetail()
        {
            var result = serv.RandomMatrix();
            Assert.IsInstanceOfType(result, typeof(OperationDetail));
        }

        [TestMethod]
        public void ExportMatrixReturnOperationDetail()
        {
            var result = serv.ExportMatrixCvs("", new MatrixViewModel());
            Assert.IsInstanceOfType(result, typeof(OperationDetail));
            Assert.IsInstanceOfType(result.ReturnedValue, typeof(string));
        }

        [TestMethod]
        public void LoadMatrixCvsReturnOperationDetail()
        {
            var result = serv.LoadMatrixCvs(new MatrixViewModel());
            Assert.IsInstanceOfType(result, typeof(OperationDetail));
            Assert.IsInstanceOfType(result.ReturnedValue, typeof(MatrixViewModel));
        }

        [TestMethod]
        public void TransformMatrixReturnOperationDetail()
        {
            var result = serv.TransformMatrix(new MatrixViewModel());
            Assert.IsInstanceOfType(result, typeof(OperationDetail));
            Assert.IsInstanceOfType(result.ReturnedValue, typeof(MatrixViewModel));
        }

        [TestMethod]
        public void ErrorExportReturnOperationDetailFalse()
        {
            var result = serv.ExportMatrixCvs("#$#%#@$@#$@", new MatrixViewModel());

            Assert.AreEqual(false,result.Succeed);
            Assert.IsInstanceOfType(result.ReturnedValue, typeof(string));
        }

        [TestMethod]
        public void ErrorLoadReturnOperationDetailFalse()
        {
            var result = serv.LoadMatrixCvs(new MatrixViewModel() { FileObj = null });

            Assert.AreEqual(false, result.Succeed);
            Assert.IsInstanceOfType(result.ReturnedValue,typeof(MatrixViewModel));
        }
    }
}
