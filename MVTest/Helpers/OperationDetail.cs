﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVTest.Helpers
{
    public class OperationDetail
    {
        public bool Succeed{get;set;}
        public string Message { get; set; }
        public object ReturnedValue { get; set; }

        public OperationDetail(bool Succeed, string Message, object ReturnedValue)
        {
            this.Succeed = Succeed;
            this.Message = Message;
            this.ReturnedValue = ReturnedValue;
        }
    }
}