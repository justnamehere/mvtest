﻿using MVTest.Helpers;
using MVTest.Models;
using MVTest.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MVTest.Services.Interfaces
{
    public interface IMatrixService
    {
        OperationDetail LoadMatrixCvs(MatrixViewModel model);
        OperationDetail TransformMatrix(MatrixViewModel model);
        OperationDetail RandomMatrix();
        OperationDetail ExportMatrixCvs(string path, MatrixViewModel model);
    }
}
