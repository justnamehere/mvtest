﻿using MVTest.Helpers;
using MVTest.Models;
using MVTest.Models.ViewModel;
using MVTest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;

namespace MVTest.Services
{
    public class MatrixService : IMatrixService
    {
        public OperationDetail ExportMatrixCvs(string path, MatrixViewModel model)
        {
            try
            {
                var random = new Random();
                var name = Path.Combine(path,string.Concat(random.Next().ToString(), ".cvs"));                
                var matrix = model.MatrixView;
                var file_cvs = File.Open(name, FileMode.Create,FileAccess.ReadWrite,FileShare.ReadWrite);
                var buffer = string.Empty;
                for(int i = 0; i < matrix.Columns; i++)
                {
                    var temp_row = string.Empty;
                    for(int j=0; j < matrix.Rows; j++)
                    {
                        temp_row += matrix.Values[i][j].ToString();
                        temp_row += ',';
                    }
                    temp_row += '\n';
                    buffer += temp_row;
                }

                file_cvs.Write(Encoding.ASCII.GetBytes(buffer), 0, buffer.Length);
                file_cvs.Close();

                return new OperationDetail(true, "File exported", name);
            }
            catch (Exception)
            {
                return new OperationDetail(false, "Error during exporting", "");
            }
        }

        public OperationDetail LoadMatrixCvs(MatrixViewModel model)
        {
            try
            {
                if(model.FileObj == null)
                    return new OperationDetail(false, "Upload the file", new MatrixViewModel { MatrixView = new Matrix(5, 5) });
                               
                var lines = ReadInList(model.FileObj.InputStream);
                var matrix = ListToMatrix(lines);
                var result = new OperationDetail(true, "Matrix loaded", new MatrixViewModel { MatrixView = matrix });

                return result;
            }
            catch (Exception ex)
            {
                return new OperationDetail(false, ex.Message, new MatrixViewModel { MatrixView = new Matrix(5, 5) });
            }
        }
        
        public OperationDetail RandomMatrix()
        {
            try
            {
                Random rand = new Random();
                var rows = rand.Next(1, 100);
                var columns = rand.Next(1, 100);

                var matrix = new Matrix(rows,columns);

                for(int i = 0; i < columns; i++)
                {
                    int[] temp_array = new int[rows];
                    for(int j = 0; j < rows; j++)
                    {
                        int temp = rand.Next(1, 100);
                        temp_array[j] = temp;
                    }
                    matrix.Values[i] = temp_array;
                }

                var result = new OperationDetail(true, "Matrix created", new MatrixViewModel { MatrixView = matrix });

                return result;
            }
            catch (Exception ex)
            {
                var result = new OperationDetail(false, ex.Message, new MatrixViewModel { MatrixView = new Matrix(5, 5) });
                return result;
            }
        }

        public OperationDetail TransformMatrix(MatrixViewModel model)
        {
            try
            {
                var matrix = model.MatrixView;
                var new_matrix = new Matrix(matrix.Columns, matrix.Rows);

                for(int i = 0; i < new_matrix.Columns; i++)
                {
                    var temp_array = new int[new_matrix.Rows];
                    for(int j = 0; j < new_matrix.Rows; j++)
                    {
                        var test = matrix.Values[j][i];
                        temp_array[j] = matrix.Values[j][i];
                    }
                    new_matrix.Values[i] = temp_array;
                }

                var result = new OperationDetail(true, "Matrix was tranformed", new MatrixViewModel { MatrixView = new_matrix });

                return result;
            }
            catch (Exception ex)
            {
                var result = new OperationDetail(false, ex.Message, new MatrixViewModel { MatrixView = new Matrix(5, 5) });

                return result;
            }
        }

        private Matrix ListToMatrix(List<string> lines)
        {
            try
            {
                int numb_columns = lines.ToArray().Length;
                int numb_in_row = GetNumbInRow(lines[0]);
                var matrix = new Matrix(numb_in_row, numb_columns);

                for (int i = 0; i < numb_columns; i++)
                {
                    var line = lines[i];
                    var arr = line.Split(',');
                    var temp_arr = new int[numb_in_row];
                    for (int j = 0; j < arr.Length; j++)
                    {
                        temp_arr[j] = int.Parse(arr[j]);
                    }
                    matrix.Values[i] = temp_arr;
                }

                return matrix;
            }
            catch (Exception)
            {
                return new Matrix(1, 1);
            }
        }

        private List<string> ReadInList(Stream stream)
        {
            try
            {
                var file_cvs = new StreamReader(stream);
                var lines = new List<string>();
                string line = String.Empty;
                while ((line = file_cvs.ReadLine()) != null)
                {
                    lines.Add(line);
                }
                file_cvs.Close();

                return lines;
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        private int GetNumbInRow(string line)
        {
            try
            {
                var test = line.Split(',');

                return test.Length;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}