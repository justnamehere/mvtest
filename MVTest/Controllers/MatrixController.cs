﻿using MVTest.Models;
using MVTest.Models.ViewModel;
using MVTest.Services.Interfaces;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace MVTest.Controllers
{
    public class MatrixController : Controller
    {
        private readonly IMatrixService service;

        public MatrixController(IMatrixService service)
        {
            this.service = service;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new MatrixViewModel());
        }

        [HttpPost]
        public ActionResult ExportMatrix(MatrixViewModel model)
        {
            var result = service.ExportMatrixCvs(Server.MapPath("~/App_Data"), model);
            if (result.Succeed)
            {                
                return File((string)result.ReturnedValue, "application/vnd.ms-excel", "matrix.csv");
            }

            return View("Index",model);
        }

        [HttpPost]
        public ActionResult LoadMatrix(MatrixViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = service.LoadMatrixCvs(model);
                if (result.Succeed)
                    return View("Index", result.ReturnedValue);
            }            
            
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult RandomMatrix()
        {
            var result = service.RandomMatrix();
            if (result.Succeed)
            {
                return View("Index", result.ReturnedValue);
            }

            return View("Index", new MatrixViewModel());
        }

        [HttpPost]
        public ActionResult TransformMatrix(MatrixViewModel model)
        {
            var result = service.TransformMatrix(model);
            if (result.Succeed)
            {
                ModelState.Clear();
                return View("Index",result.ReturnedValue);
            }

            return View("Index",new MatrixViewModel());
        }
    }
}