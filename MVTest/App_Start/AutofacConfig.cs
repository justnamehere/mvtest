﻿using Autofac;
using Autofac.Integration.Mvc;
using MVTest.Services;
using MVTest.Services.Interfaces;
using System.Web.Mvc;

namespace MVTest.App_Start
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {            
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<MatrixService>().As<IMatrixService>();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}