﻿using System.Web.Optimization;

namespace MVTest
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Assets/scripts/jquery-{version}.js",
                        "~/Assets/scripts/jquery-ui-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/Assets/custom/js").Include(                        
                        "~/Assets/scripts/custom.js",
                        "~/Assets/scripts/tree/Tree.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
                        "~/Assets/scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Assets/scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Assets/scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Assets/scripts/bootstrap.js",
                      "~/Assets/scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Assets/custom/css").Include(
                      "~/Assets/css/site.css",
                       "~/Assets/css/custom.css",
                       "~/Assets/css/treestyle.css"));

            bundles.Add(new StyleBundle("~/bundles/jquery-ui/css").Include(
                "~/Assets/css/themes/base/jquery-ui.min.css",
                "~/Assets/css/themes/base/all.css"));

            bundles.Add(new StyleBundle("~/Assets/metronik/css").Include(
                      "~/Assets/metronik/global/plugins/font-awesome/css/font-awesome.min.css",
                      "~/Assets/metronik/global/plugins/bootstrap/css/bootstrap.min.css",
                      "~/Assets/metronik/global/plugins/simple-line-icons/simple-line-icons.min.css",
                      "~/Assets/metronik/global/plugins/uniform/css/uniform.default.css",
                      "~/Assets/metronik/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                      "~/Assets/metronik/global/css/components.css",
                      "~/Assets/metronik/global/css/plugins.css",

                      "~/Assets/metronik/layouts/layout4/css/layout.css",
                      "~/Assets/metronik/layouts/layout4/css/themes/light.css",
                      "~/Assets/metronik/global/plugins/select2/css/select2.min.css",
                      "~/Assets/metronik/pages/css/login-4.min.css"
                      ));

            bundles.Add(new ScriptBundle("~/Assets/metronik/js").Include(
                      "~/Assets/metronik/global/plugins/bootstrap/js/bootstrap.min.js",
                      "~/Assets/metronik/global/plugins/js.cookie.min.js",
                      "~/Assets/metronik/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                      "~/Assets/metronik/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                      "~/Assets/metronik/global/plugins/jquery.blockui.min.js",
                      "~/Assets/metronik/global/plugins/uniform/jquery.uniform.min.js",
                      "~/Assets/metronik/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",                     
                      "~/Assets/metronik/global/plugins/jquery.sparkline.min.js",                     
                      "~/Assets/metronik/global/scripts/app.min.js",                      

                      "~/Assets/metronik/layouts/layout4/scripts/layout.min.js",                      
                      "~/Assets/metronik/layouts/global/scripts/quick-sidebar.min.js",

                      "~/Assets/metronik/global/plugins/bootbox/bootbox.min.js"));

            bundles.Add(new ScriptBundle("~/Assets/metronik/login/js").Include(                
                      "~/Assets/metronik/global/plugins/bootstrap/js/bootstrap.min.js",
                      "~/Assets/metronik/global/plugins/js.cookie.min.js",
                      "~/Assets/metronik/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                      "~/Assets/metronik/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                      "~/Assets/metronik/global/plugins/jquery.blockui.min.js",
                      "~/Assets/metronik/global/plugins/uniform/jquery.uniform.min.js",
                      "~/Assets/metronik/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",

                      "~/Assets/metronik/global/plugins/jquery-validation/js/jquery.validate.min.js",
                      "~/Assets/metronik/global/plugins/jquery-validation/js/additional-methods.min.js",
                      "~/Assets/metronik/global/plugins/backstretch/jquery.backstretch.min.js",
                      
                      "~/Assets/metronik/global/scripts/app.min.js",
                      "~/Assets/metronik/pages/scripts/login-4.js"));
        }
    }
}
